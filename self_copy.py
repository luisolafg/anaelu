import shutil, os
from distutils import sysconfig

if( __name__ == "__main__" ):

    inc_path = sysconfig.get_python_inc()
    print "\n\n sysconfig.get_python_inc() =", inc_path

    lst_inc_path = inc_path.split("/")
    print "lst_inc_path =", lst_inc_path

    miniconda_path = "/".join(lst_inc_path[0:-2])
    print "miniconda_path =", miniconda_path

    cwd = os.getcwd()
    print "cwd =", cwd

    shutil.copytree(cwd, "dist/anaelu_main")
    print "Copied ... 1"
    shutil.copytree(miniconda_path, "dist/conda_bundle", symlinks=True)
    print "Copied ... 2"

    tup_str2write = (
        "echo \" \"",
        "echo \"Running autogenerated Anaelu install script ...\"",
        "echo \" \"",
        "echo \"step 1:\"",
        "echo \" \"",
        "echo \"Working on Python packaged with Conda \"",
        "cd conda_bundle/bin/",
        "echo \" \"",
        "echo \"path to Conda binary = $(pwd)\"",
        "echo \" \"",
        "echo \" \"",
        "export PATH=$(pwd):$PATH",
        "echo \" \"",
        "echo \"step 2:\"",
        "echo \"Working on Anaelu source root: \"",
        "cd ../../anaelu_main/",
        "echo \" \"",
        "echo \"path to Anaelu source = $(pwd)\"",
        "echo \" \"",
        "python gen_path.py $(pwd) add_py",
        "echo \" \"",
        "echo \" ... Done\"",
        "echo \" \"",
        "echo \" \"",
        "echo \"Anaelu bundle was succesfully installed\"",
        "echo \" \"",
        "echo \" \"",
        "echo \"To get Anaelu ready to run, just add the following line:\"",
        "echo \" \"",
        "echo \"source $(pwd)/setpath.sh\"",
        "echo \" \"",
        "echo \"to either ~/.bashrc   or  ~/.profile start scripts, depending \"",
        "echo \"on your distribution. This will override your Python \"",
        "echo \"installation. If you prefer to get Anaelu ready to run\"",
        "echo \"on a temporal state just type:\"",
        "echo \" \"",
        "echo \"source $(pwd)/setpath.sh\"",
        "echo \" \"",
        "echo \"and the configuration will last only the current session\"",
        "echo \" \"",
        "echo \" \""
    )

    myfile = open("dist/install_anaelu.sh", "w")
    for single_str in tup_str2write:
        str2write = single_str + "\n"
        myfile.write(str2write)

    myfile.close()
