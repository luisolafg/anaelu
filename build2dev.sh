# This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

TMP_ROOT=$(pwd)
echo "TMP_ROOT = $TMP_ROOT"
echo " "
echo "Building ANAELU suite by compiling from source"
echo " "
echo " "

cd command_line_utilities/
./build_crysfml_n_anaelu.sh
cd $TMP_ROOT
echo " "
echo "creating build_apps dir and copying Anaelu_gfortran to it"
echo " "

rm -rf build_apps
mkdir build_apps
cp command_line_utilities/anaelu_cli_tools/anaelu_calc_xrd build_apps/anaelu_calc_xrd
cp command_line_utilities/anaelu_cli_tools/anaelu_calc_mask build_apps/anaelu_calc_mask

echo " "
echo "Building CLI image transformation tools"
echo " "

cd command_line_utilities/mar_img_convert/

./gfortran_lin_float64_comp.sh
cd $TMP_ROOT
cp command_line_utilities/mar_img_convert/mar_img/bin2float.r build_apps/

cd command_line_utilities/img_treatment_tools
echo " "
echo "removing previous builds"
echo " "
rm *.so *.pyc

./comp_f2py_mult_tools.sh
./comp_f2py_cut_tool.sh

cd $TMP_ROOT

cp command_line_utilities/img_treatment_tools/img_rot.sh build_apps/anaelu_img_rot
cp command_line_utilities/img_treatment_tools/img_cut.sh build_apps/anaelu_img_cut
cp command_line_utilities/img_treatment_tools/img_scale.sh build_apps/anaelu_img_scale
cp command_line_utilities/img_treatment_tools/img_smooth.sh build_apps/anaelu_img_smooth
cp command_line_utilities/img_treatment_tools/img_compare.sh build_apps/anaelu_dif_res
cp command_line_utilities/img_treatment_tools/img_add.sh build_apps/anaelu_img_add
cp command_line_utilities/img_treatment_tools/img_mark.sh build_apps/anaelu_img_mark_rect
cp command_line_utilities/img_treatment_tools/img_convert.sh build_apps/anaelu_fabio2edf

cd command_line_utilities
./build_cif_to_cfl_converter.sh
cd $TMP_ROOT
cp command_line_utilities/format_tools/anaelu_cif2cfl build_apps/anaelu_cif2cfl

#cd command_line_utilities/anaelu_cli_tools           #line 44 to 47 are the added commands for the mask
#./make_2d_mask_builder.sh                            #it works fine in my anaelu_test
#cd $TMP_ROOT                                         #hope it will work in the Ananelu

echo " "
cp gui/anaelu_gui build_apps/

echo "genrating set path with $TMP_ROOT hard coded"
echo " "
python gen_path.py $TMP_ROOT do_conda
echo " "
echo "done all"
