#!/usr/bin/python
#
# Copyright (C) 2013-2017  CIMAV / DLS
#
# Authors: Luis Fuentes Montero (DLS)
#          Eduardo Villalobos Portillo (CIMAV)
#          Diana C. Burciaga (CIMAV)
#          Luis E. Fuentes Cobas (CIMAV)
#
# Contributors: Juan Rodrgiguez Carvajal (ILL)
#               Jose Alfredo (S. Queretaro)
#               Javier (ULL)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
import numpy as np
import math
import fabio

#def read_bin_file(path_to_img = "my_bin_img.bin", fortran_dat = False):
def read_bin_file(path_to_img = "my_bin_img.bin"):

    '''function to read de image in .bin format'''
    print "reading ", path_to_img, " file"

    data_in = np.fromfile(path_to_img, dtype=np.float64)
    lng_fl = len(data_in)
    cuad_lng = int(math.sqrt(len(data_in)))

    xres = cuad_lng
    yres = cuad_lng
    print "xres, yres =", xres, yres
    read_data = data_in.reshape((xres, yres))

    old_way = '''
    read_data = data_in.reshape((xres, yres), order="FORTRAN")
    if(fortran_dat == True):
        read_data = np.transpose(read_data)
    '''
    print "Done reading 2D data"
    return read_data


def read_file_w_fabio(path_to_img =
                      "AAL_024_Co_dep_30sec_th1_col_ancho_a_05050256_0001.tif"):

    img_xmap = fabio.open(path_to_img)
    file_in_ext = path_to_img[-3:]
    print "\n\n fabio(ext) =", file_in_ext

    arr_raw = img_xmap.data

    attempt_to_coorect_01 = '''
    if( file_in_ext == "edf" ):
        print "applying transpose to edf file"
        arr_raw = np.transpose(arr_raw)
    '''

    return arr_raw


def read_file(path_to_img = "PATH_TO_MY_IMG.txt"):

    '''read line by line the image file to find resolution'''

    print "readin ", path_to_img, " file"

    myfile = open(path_to_img, "r")

    all_lines = myfile.readlines()
    myfile.close()
    n=0

    xres = 2300
    yres = 2300

    n_line = 0

    for line in all_lines:
        n_line += 1
        if( n_line < 10 ):
            print "____________________________________________________"

            print line
            print "comparing:"
            print line[:32].lower()
            print "with:"
            print " number of pixels in x direction"
        if( line[:32].lower() == " number of pixels in x direction" ):
            print "Found the X resolution"
            print "asc to convert =", line[36:]
            xres = int(line[36:])
            print " X =", xres

        elif( line[:32].lower() == " number of pixels in y direction" ):
            print "Found the Y resolution"
            print "asc to convert =", line[36:]
            yres = int(line[36:])
            print " Y =", yres



    a=np.zeros((xres,yres))

    x=0
    y=yres - 1
    for line in all_lines:
        n+=1
        if n>6:
            a[x,y]=float(line)
            if x>=xres-1:
                x = -1
                y -= 1
            x += 1
        else:
            print n, line
    return a


if(__name__ == "__main__"):
    pth = "/home/lui/f90lafg_code/testing_n_learning/luiso_s/img_formats_test/f90_read/Test.raw"
    img_arr = read_bin_file(pth)
    plott_img(img_arr)
