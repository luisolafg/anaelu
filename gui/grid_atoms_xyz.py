#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import wx.grid as gridlib


class AtomData(object):


    '''functionless data class'''
    def __init__(self):

        self.Zn  = None
        self.Lbl = None
        self.x   = None
        self.y   = None
        self.z   = None
        self.b   = None

def read_cfl_atoms(my_file_path = "cfl_out.cfl"):

    '''opening the .cfl file to read the atoms data'''
    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;=:':    # filtering unwanted data in the .cfl file as ,:=;
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():    # filtering the commented lines
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)


    lst_pos = []
    for pos, dat in enumerate(lst_dat):

        if(dat[0:4] == "Atom"):

            lst_pos.append(pos)


        atm_lst = []
        for n_i, pos in enumerate(lst_pos):
            atm = AtomData()
            atm.Zn = lst_dat[pos + 1]
            atm.Lbl = lst_dat[pos + 2]
            atm.x = lst_dat[pos + 3]
            atm.y = lst_dat[pos + 4]
            atm.z = lst_dat[pos + 5]
            try:
                atm.b = lst_dat[pos + 6]
                if(atm.b == "Atom"):
                    atm.b = None
            except:
                atm.b = None
            atm_lst.append(atm)

    return atm_lst


class MyGenGrid(gridlib.Grid):

    def __init__(self, parent):

        """Constructor of atoms grid"""
        super(MyGenGrid, self).__init__(parent)
        self.parent_widget = parent
        self.nm_atoms = 1
        self.CreateGrid(numRows = self.nm_atoms, numCols = 6)

        self.SetColLabelValue(0, " Z ")
        self.SetColLabelValue(1, "Label")
        self.SetColLabelValue(2, "x Pos")
        self.SetColLabelValue(3, "y Pos")
        self.SetColLabelValue(4, "z Pos")
        self.SetColLabelValue(5, "DWF")
        self.Bind(gridlib.EVT_GRID_CELL_LEFT_CLICK, self.OnCellLeftClick)
        self.Bind(gridlib.EVT_GRID_CELL_CHANGE, self.OnCellChange)


    def OnCellLeftClick(self, evt):

        evt.Skip()

    def OnCellChange(self, evt):

        value = self.GetCellValue(evt.GetRow(), evt.GetCol())

    def set_my_values(self, lst_data):

        #clean out the grid in full before starting to fill with data
        self.nm_atoms = self.GetNumberRows()
        if(self.nm_atoms > len(lst_data)):
            dif = self.nm_atoms - len(lst_data)
            self.del_row(dif)

        elif(self.nm_atoms < len(lst_data)):
            dif = len(lst_data) - self.nm_atoms
            self.appn_row(dif)

        # filling grid with data
        for row, tmp_atom in enumerate(lst_data):

            if(tmp_atom.Zn != None):
                self.SetCellValue(row, 0, tmp_atom.Zn )

            if(tmp_atom.Lbl != None):
                self.SetCellValue(row, 1, tmp_atom.Lbl)

            if(tmp_atom.x != None):
                self.SetCellValue(row, 2, tmp_atom.x  )

            if(tmp_atom.y != None):
                self.SetCellValue(row, 3, tmp_atom.y  )

            if(tmp_atom.z != None):
                self.SetCellValue(row, 4, tmp_atom.z  )

            if(tmp_atom.b != None):
                self.SetCellValue(row, 5, tmp_atom.b  )
        self.nm_atoms = self.GetNumberRows()

    def get_my_values(self):
        '''reading the atoms values from the gui grid'''
        lst_data = []
        print self.nm_atoms
        for row in xrange(self.nm_atoms):

            tmp_atom = AtomData()
            tmp_atom.Zn  = str(self.GetCellValue(row, 0))
            tmp_atom.Lbl = str(self.GetCellValue(row, 1))
            tmp_atom.x   = str(self.GetCellValue(row, 2))
            tmp_atom.y   = str(self.GetCellValue(row, 3))
            tmp_atom.z   = str(self.GetCellValue(row, 4))
            tmp_atom.b   = str(self.GetCellValue(row, 5))
            lst_data.append(tmp_atom)

        return lst_data

    def appn_row(self, n_dif = 1):
        self.AppendRows(n_dif)
        self.nm_atoms = self.GetNumberRows()
        self.upd_ref_lay()

    def del_row(self, n_dif = 1):
        if( n_dif == 1 ):
            pos_to_del = self.GetNumberRows() - 1
        else:
            pos_to_del = 0

        self.DeleteRows(pos = pos_to_del, numRows = n_dif, updateLabels = True)
        self.nm_atoms = self.GetNumberRows()
        self.upd_ref_lay()

    def upd_ref_lay(self):
        self.parent_widget.v_sizer.Fit(self)
        tst_off = '''
        self.Layout()
        self.parent_widget.Layout()
        self.parent_widget.parent_widget.Layout()
        self.ForceRefresh()
        self.parent_widget.Refresh()
        self.parent_widget.parent_widget.Refresh()
        '''
        self.ForceRefresh()
        self.parent_widget.parent_widget.SendSizeEvent()

        print "upd_ref_lay(self)"

class AtomsPanel(wx.Panel):
    '''creating the atoms panel for the gui'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(AtomsPanel, self).__init__(parent)

        self.parent_widget = parent

        self.myGrid = MyGenGrid(self)
        self.myGrid.AutoSize()
        self.v_sizer = wx.BoxSizer(wx.VERTICAL)
        self.h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.v_sizer.Add(self.myGrid, 0, wx.ALL|wx.EXPAND, 5)

        self.add_button = wx.Button(self, label="Add row")
        self.del_button = wx.Button(self, label="Del row")
        self.h_sizer.Add(self.add_button, 0, wx.ALL|wx.EXPAND, 5)
        self.h_sizer.Add(self.del_button, 0, wx.ALL|wx.EXPAND, 5)
        self.v_sizer.Add(self.h_sizer, 0, wx.ALL|wx.EXPAND, 5)

        self.add_button.Bind(wx.EVT_BUTTON, self.OnAddButt)
        self.del_button.Bind(wx.EVT_BUTTON, self.OnDelBut)

        self.SetSizer(self.v_sizer)


    def OnLoadButt(self, event, ld_path = "My_Cfl.cfl"):
        self.atm_lst = read_cfl_atoms(ld_path)
        self.myGrid.set_my_values(self.atm_lst)

    def OnAddButt(self, event):
        self.myGrid.appn_row()

    def OnDelBut(self, event):
        self.myGrid.del_row()

    def write_cfl_athom(self, dto_lts, myfile = None):


        if(myfile == None):
            myfile = open("cfl_out.cfl", "w")


        for atm in dto_lts:
            wrstring = "Atom   "
            myfile.write(wrstring);

            wrstring = atm.Zn + "  " + atm.Lbl + "  " + \
                        atm.x + "   " + atm.y + "   " + atm.z + "   " + \
                        atm.b + "   " + "\n"

            myfile.write(wrstring);

        myfile.close()
