#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import numpy as np
import time

class np2bmp_heat(object):
    def __init__(self):

        self.red_byte = np.empty( (255 * 3), 'int')
        self.green_byte = np.empty( (255 * 3), 'int')
        self.blue_byte = np.empty( (255 * 3), 'int')

        for i in xrange(255):
            self.red_byte[i] = i
            self.green_byte[i + 255] = i
            self.blue_byte[i + 255 * 2 ] = i

        self.red_byte[255:255 * 3] = 255
        self.green_byte[0:255] = 0
        self.green_byte[255 * 2 : 255 * 3] = 255
        self.blue_byte[0:255 * 2] = 0

        self.blue_byte[764] = 255
        self.red_byte[764] = 255
        self.green_byte[764] = 255


    def img_2d_rgb(self, data2d = None, invert = False,
                   sqrt_scale = False, i_min_max = [None, None]):

        data2d_min = data2d.min()
        data2d_max = data2d.max()

        self.local_min_max = [float(data2d_min), float(data2d_max)]
        if(i_min_max == [None, None]):
            print"no max and min provided"

        else:
            if(i_min_max[0] < data2d_min):
                data2d_min = i_min_max[0]

            if(i_min_max[1] > data2d_max):
                data2d_max = i_min_max[1]

        self.width = np.size( data2d[0:1, :] )
        self.height = np.size( data2d[:, 0:1] )

        data2d_pos = data2d[:,:] - data2d_min + 1.0
        data2d_pos_max = data2d_pos.max()

        calc_pos_max = data2d_max - data2d_min + 1.0
        if(calc_pos_max > data2d_pos_max):
            data2d_pos_max = calc_pos_max

        div_scale = 764.0 / data2d_pos_max

        #print "calc_pos_max =", calc_pos_max

        #print "data2d_pos_max =", data2d_pos_max, "\n"

        data2d_scale = np.multiply(data2d_pos, div_scale)

        if(sqrt_scale == True):
            for x in np.nditer(data2d_scale[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = np.sqrt(x[...])

            div_scale = 764.0 / data2d_scale.max()
            data2d_scale = np.multiply(data2d_scale, div_scale)

        if(invert == True):
            for x in np.nditer(data2d_scale[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = 764.0 - x[...]

        img_array = np.empty( (self.height ,self.width, 3),'uint8')

        img_array_r = np.empty( (self.height, self.width), 'int')
        img_array_g = np.empty( (self.height, self.width), 'int')
        img_array_b = np.empty( (self.height, self.width), 'int')

        scaled_i = np.empty( (self.height, self.width), 'int')
        scaled_i[:,:] = data2d_scale[:,:]

        img_array_r[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.red_byte[x]

        img_array_g[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.green_byte[x]

        img_array_b[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.blue_byte[x]

        img_array[:, :, 0] = img_array_r[:,:]
        img_array[:, :, 1] = img_array_g[:,:]
        img_array[:, :, 2] = img_array_b[:,:]

        image = wx.EmptyImage(self.width, self.height)
        image.SetData( img_array.tostring())

        scale = 5.0
        NewW = int(self.width * scale)
        NewH = int(self.height * scale)
        image = image.Scale(NewW, NewH, wx.IMAGE_QUALITY_NORMAL)

        self.i_min_max = [data2d_min, data2d_max]

        return image


class np2bmp_diff(object):
    def __init__(self):
        self.max_i_scale = 255 * 2 - 1

        self.red_byte = np.empty( (255 * 2), 'int')
        self.green_byte = np.empty( (255 * 2), 'int')
        self.blue_byte = np.empty( (255 * 2), 'int')

        self.red_byte[:] = 255
        self.green_byte[:] = 255
        self.blue_byte[:] = 255

        for i in xrange(255):
            self.red_byte[i] = i
            self.green_byte[i] = i

            self.green_byte[i + 255] = 255 - i
            self.blue_byte[i + 255] = 255 - i

        self.blue_byte[self.max_i_scale] = 0
        self.red_byte[self.max_i_scale] = 0
        self.green_byte[self.max_i_scale] = 255

        ##########################################################

        self.red_byte_inv = np.empty( (255 * 2), 'int')
        self.green_byte_inv = np.empty( (255 * 2), 'int')
        self.blue_byte_inv = np.empty( (255 * 2), 'int')

        self.red_byte_inv[:] = 0
        self.green_byte_inv[:] = 0
        self.blue_byte_inv[:] = 0

        for i in xrange(255):
            self.red_byte_inv[i] = 255 - i
            self.green_byte_inv[i + 255] = i


        self.blue_byte_inv[self.max_i_scale] = 0
        self.red_byte_inv[self.max_i_scale] = 0
        self.green_byte_inv[self.max_i_scale] = 255

        ##########################################################

        self.red_byte_alt = np.empty( (255 * 2), 'int')
        self.green_byte_alt = np.empty( (255 * 2), 'int')
        self.blue_byte_alt = np.empty( (255 * 2), 'int')

        self.red_byte_alt[:] = 255
        self.green_byte_alt[:] = 255
        self.blue_byte_alt[:] = 255

        for i in xrange(255):
            self.red_byte_alt[i] = i
            self.blue_byte_alt[i] = i

            self.blue_byte_alt[i + 255] = 255 - i
            self.green_byte_alt[i + 255] = 255 - i

        self.blue_byte_alt[self.max_i_scale] = 255
        self.red_byte_alt[self.max_i_scale] = 0
        self.green_byte_alt[self.max_i_scale] = 0

        ##################################################################

        self.red_byte_inv_alt = np.empty( (255 * 2), 'int')
        self.green_byte_inv_alt = np.empty( (255 * 2), 'int')
        self.blue_byte_inv_alt = np.empty( (255 * 2), 'int')

        self.red_byte_inv_alt[:] = 0
        self.green_byte_inv_alt[:] = 0
        self.blue_byte_inv_alt[:] = 0

        for i in xrange(255):
            self.red_byte_inv_alt[i] = 255 - i
            self.blue_byte_inv_alt[i + 255] = i


        self.green_byte_inv_alt[self.max_i_scale] = 0
        self.red_byte_inv_alt[self.max_i_scale] = 0
        self.blue_byte_inv_alt[self.max_i_scale] = 255

    def img_2d_rgb(self, data2d = None, invert = False,
                   sqrt_scale = False, i_min_max = [None, None]):

        data2d_min = data2d.min()
        data2d_max = data2d.max()

        self.local_min_max = [float(data2d_min), float(data2d_max)]
        if(i_min_max == [None, None]):
            print "no max and min provided"

        else:
            print "ignoring any max and min provided"


        print "\n data2d_min, data2d_max =", data2d_min, data2d_max, "\n"

        if(abs(data2d_max) > abs(data2d_min)):
            big_abs = abs(data2d_max)

        else:
            big_abs = abs(data2d_min)

        data2d_min = - big_abs
        data2d_max = big_abs

        #print "\n data2d_min, data2d_max =", data2d_min, data2d_max, "\n"

        self.width = np.size( data2d[0:1, :] )
        self.height = np.size( data2d[:, 0:1] )

        data2d_pos = data2d[:,:] - data2d_min + 1.0
        data2d_pos_max = data2d_pos.max()

        calc_pos_max = data2d_max - data2d_min + 1.0
        if(calc_pos_max > data2d_pos_max):
            data2d_pos_max = calc_pos_max

        div_scale = self.max_i_scale / data2d_pos_max

        #print "calc_pos_max =", calc_pos_max

        print"data2d_pos_max =", data2d_pos_max, "\n"

        data2d_scale = np.multiply(data2d_pos, div_scale)
        '''
        if(sqrt_scale == True):
            for x in np.nditer(data2d_scale[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = np.sqrt(x[...])

            div_scale = self.max_i_scale / data2d_scale.max()
            data2d_scale = np.multiply(data2d_scale, div_scale)
        '''

        img_array = np.empty( (self.height ,self.width, 3),'uint8')

        img_array_r = np.empty( (self.height, self.width), 'int')
        img_array_g = np.empty( (self.height, self.width), 'int')
        img_array_b = np.empty( (self.height, self.width), 'int')

        scaled_i = np.empty( (self.height, self.width), 'int')
        scaled_i[:,:] = data2d_scale[:,:]


        if(invert == True and sqrt_scale == True):
            img_array_r[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.red_byte_inv[x]

            img_array_g[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.green_byte_inv[x]

            img_array_b[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.blue_byte_inv[x]

        elif(invert == False and sqrt_scale == True):
            img_array_r[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.red_byte_inv_alt[x]

            img_array_g[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.green_byte_inv_alt[x]

            img_array_b[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.blue_byte_inv_alt[x]

        elif(invert == True and sqrt_scale == False):
            img_array_r[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.red_byte_alt[x]

            img_array_g[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.green_byte_alt[x]

            img_array_b[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.blue_byte_alt[x]

        else:
            img_array_r[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.red_byte[x]

            img_array_g[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.green_byte[x]

            img_array_b[:,:] = scaled_i[:,:]
            for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = self.blue_byte[x]

        img_array[:, :, 0] = img_array_r[:,:]
        img_array[:, :, 1] = img_array_g[:,:]
        img_array[:, :, 2] = img_array_b[:,:]

        image = wx.EmptyImage(self.width, self.height)
        image.SetData( img_array.tostring())

        scale = 5.0
        NewW = int(self.width * scale)
        NewH = int(self.height * scale)
        image = image.Scale(NewW, NewH, wx.IMAGE_QUALITY_NORMAL)

        self.i_min_max = [data2d_min, data2d_max]

        return image
