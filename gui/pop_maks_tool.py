import wx
import numpy as np

from np_nditer_RGB.looping import np2bmp_heat
from np_nditer_RGB.bitmap_from_numpy_hot_colour_palete import build_np_img

class PaintAreaPanel(wx.Panel):

    c1 = None
    c2 = None

    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.Bind(wx.EVT_PAINT, self.Drawing)
        self.lst_rec = []

    def intro_n_show(self, data2d_in, fr_scale_in):
        self.data2d_pan = data2d_in
        self.fr_scale_pan = fr_scale_in
        self.bmp_dat = np2bmp_heat()
        self.get_my_bmp()
        self.Show(True)
        self.Layout()
        self.Drawing()

    def get_my_bmp(self, new_sqrt_scale = False):

        tmp_img = self.bmp_dat.img_2d_rgb(
            data2d = self.data2d_pan,
            invert = False, sqrt_scale = new_sqrt_scale
        )

        n_y = np.size( self.data2d_pan[0:1, :] )
        n_x = np.size( self.data2d_pan[:, 0:1] )
        my_img = tmp_img.Scale(
            n_y * self.fr_scale_pan, n_x * self.fr_scale_pan,
            wx.IMAGE_QUALITY_NORMAL
        )
        self.my_bmp = wx.BitmapFromImage(my_img)


    def Drawing(self, event = None):
        self.DevCont = wx.PaintDC(self)

        self.DevCont.Clear()
        self.DevCont.DrawBitmap(self.my_bmp, 1, 1)

        dc = wx.PaintDC(self)
        dc.SetPen(wx.Pen('BLUE', 3))
        dc.SetBrush(wx.Brush("BLACK", wx.TRANSPARENT))

        for rect_wx_tup in self.lst_rec:
            dc.DrawRectangle(rect_wx_tup[0].x, rect_wx_tup[0].y,
                             rect_wx_tup[1].x - rect_wx_tup[0].x, rect_wx_tup[1].y - rect_wx_tup[0].y)

        if(self.c1 != None and self.c2 != None):
            dc.DrawRectangle(self.c1.x, self.c1.y,
                             self.c2.x - self.c1.x, self.c2.y - self.c1.y)

        self.Layout()

    def OnMouseMove(self, event):
        self.c2 = event.GetPosition()
        self.Refresh()

    def OnMouseDown(self, event):
        self.c1 = event.GetPosition()
        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))

    def OnMouseUp(self, event):
        self.lst_rec.append((self.c1, self.c2))
        self.c1 = None
        self.c2 = None
        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))


class MyFrame(wx.Frame):
    def __init__(self,(width, height)):
        super(MyFrame, self).__init__( None, -1, "Mask Tool", size = (width,height))

    def build_in(self, data2d_in, fr_scale_in, my_parent):
        self.parent = my_parent

        ApplyBtn  = wx.Button(self, wx.ID_ANY, 'Apply Mask\n')
        self.Bind(wx.EVT_BUTTON, self.Apply, ApplyBtn)

        CancelBtn  = wx.Button(self, wx.ID_ANY, 'Cancel Mask\n')
        self.Bind(wx.EVT_BUTTON, self.Cancel, CancelBtn)

        self.chk_sqrt = wx.CheckBox(self, label = "sqrt(i)")
        self.Bind(wx.EVT_CHECKBOX,
                  self.onCheck_invert,
                  self.chk_sqrt)

        self.mPanel = PaintAreaPanel(self)
        self.mPanel.intro_n_show(data2d_in, fr_scale_in)

        vsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        vsizer.Add(ApplyBtn, 1, wx.EXPAND)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        vsizer.Add(CancelBtn, 1, wx.EXPAND)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        vsizer.Add(self.chk_sqrt, 3, wx.EXPAND)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)

        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        hsizer.Add(vsizer, 1, wx.EXPAND)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        hsizer.Add(self.mPanel, 3, wx.EXPAND)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)


        self.SetSizer(hsizer)

    def onCheck_invert(self, event):
        print "Hi there"
        evn_chek = event.Checked()
        print "event.Checked() =", evn_chek
        print "type(event.Checked()) =", type(evn_chek)

        self.mPanel.get_my_bmp(new_sqrt_scale = evn_chek)
        self.mPanel.Drawing()

    def Apply(self, event):
        print "Apply clicked"
        print "( mPanel.lst_rec )=", self.mPanel.lst_rec

        print "mPanel.fr_scale_pan =", self.mPanel.fr_scale_pan

        self.lst_out = []

        for rect_wx_tup in self.mPanel.lst_rec:
            x1 = int(float(rect_wx_tup[0].x) / float(self.mPanel.fr_scale_pan) )
            y1 = int(float(rect_wx_tup[0].y) / float(self.mPanel.fr_scale_pan) )
            x2 = int(float(rect_wx_tup[1].x) / float(self.mPanel.fr_scale_pan) )
            y2 = int(float(rect_wx_tup[1].y) / float(self.mPanel.fr_scale_pan) )
            rect_out = (x1, y1, x2, y2)
            print "rect_out =", rect_out
            self.lst_out.append(rect_out)

        self.parent.app_mark_tool()

        self.Close()

    def Cancel(self, event):
        print "Cancel clicked"
        self.lst_out = None
        self.Close()


class MyPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        btn = wx.Button(self, label='Create New Frame')
        btn.Bind(wx.EVT_BUTTON, self.on_new_frame)
        self.frame_number = 1

    def on_new_frame(self, event):

        ################################################################
        data2d = build_np_img(height = 60, width = 130)

        n_y = np.size( data2d[0:1, :] )
        n_x = np.size( data2d[:, 0:1] )

        arr_avgr = float(n_y + n_x)
        my_avgr = 850.0
        print "arr_avgr =", arr_avgr
        print "my_avgr =", my_avgr

        fr_scale = my_avgr / arr_avgr
        print "fr_scale =", fr_scale
        fr_dim = (int(n_y * fr_scale) + 200, int(n_x * fr_scale) + 50)
        print "fr_dim =", fr_dim
        self.frame = MyFrame(fr_dim)
        self.frame.build_in(data2d, fr_scale, self)
        self.frame.Show()

    def app_mark_tool(self):
        print "self.frame.lst_out =", self.frame.lst_out

class MainFrame(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, title='Main Frame', size=(800, 600))
        panel = MyPanel(self)
        self.Show()


if __name__ == '__main__':
    app = wx.App(False)
    frame = MainFrame()
    app.MainLoop()
