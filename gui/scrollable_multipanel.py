#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx

import os
import math
import numpy as np
import wx.lib.scrolledpanel as scroll_pan

from read_2d_numpy import read_bin_file, read_file_w_fabio
from np_nditer_RGB.looping import np2bmp_heat, np2bmp_diff

from subprocess import call as shell_func


class ScrolledImg(scroll_pan.ScrolledPanel):
    '''all the functions to manipulate both xrd images (experimental and calculated)'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ScrolledImg, self).__init__(parent)

        ''' to recognize the pointer potition on the images'''
        self.Prnt_Wg = parent
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.i_min_max = [None, None]

        self.invert_img = False
        self.sqrt_scale_img = False

        self.scroll_rot = 0
        self.Bdwn = False
        self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y = 0, 0
        self.Prnt_Wg.img_scale = 0.06
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.SetAutoLayout(1)

    def set_bmp_gen(self, palette_in = 1):

        if(palette_in == 1):
            self.bmp_gen = np2bmp_heat()

        else:
            self.bmp_gen = np2bmp_diff()

        self.img = None

    def intro_img_path(self, path_to_img = None, use_fabio = False):
        '''
        reading the image from the .bin file and getting the dimensions

        or updating/repainting image/palette
        '''

        if(path_to_img != None):
            if( use_fabio == True ):
                self.data2d = read_file_w_fabio(path_to_img)

            else:
                #self.data2d = read_bin_file(path_to_img, fortran_dat = True)
                self.data2d = read_bin_file(path_to_img)

            data_shape = np.shape(self.data2d)
            try:
                self.Prnt_Wg.parent_widget.data_panel.param_panel.input_x_res.SetValue(str(data_shape[1]))
                self.Prnt_Wg.parent_widget.data_panel.param_panel.input_y_res.SetValue(str(data_shape[0]))

            except:
                print "unable to automatically set resolution"

            print "2D data shape =", data_shape

    def intro_img_path_2(self):

        self.make_image()
        self.intro_img_path_3()

    def intro_img_path_3(self):

        self.DestroyChildren()
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.set_img_n_bmp()
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.SetAutoLayout(1)

    def make_image(self, my_call_back = True):
        self.img = self.bmp_gen.img_2d_rgb(data2d = self.data2d,
                                      invert = self.invert_img,
                                      sqrt_scale = self.sqrt_scale_img,
                                      i_min_max = self.i_min_max )

        self.local_min_max = self.bmp_gen.local_min_max

        self.i_width = self.img.GetWidth()
        self.i_height = self.img.GetHeight()

        if(self.i_min_max == [None, None]):
            self.i_min_max = self.bmp_gen.i_min_max

        else:
            if(self.bmp_gen.i_min_max[0] < self.i_min_max[0]):
                self.i_min_max[0] = self.bmp_gen.i_min_max[0]

            if(self.bmp_gen.i_min_max[1] > self.i_min_max[1]):
                self.i_min_max[1] = self.bmp_gen.i_min_max[1]


        if(type(self.bmp_gen).__name__ == "np2bmp_diff"):
            print "no need to re-scale palette in all images"

        elif(my_call_back == True):
            self.Prnt_Wg.Refr_Imgs()

    def set_img_n_bmp(self):

        '''setting new dimensions of the image to control de zoom-unzoom'''

        if( self.img != None ):
            my_img = self.img.Scale(self.i_width * self.Prnt_Wg.img_scale,
                                    self.i_height * self.Prnt_Wg.img_scale,
                                    wx.IMAGE_QUALITY_NORMAL)
            my_bitmap = wx.BitmapFromImage(my_img)
            my_img.Destroy()
            self.my_st_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
            my_bitmap.Destroy()

            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
            self.my_bindings()

        else:
            load_img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
            self.Bind(wx.EVT_BUTTON, self.onLoadImgBtn, load_img_btn)
            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(load_img_btn, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)

        self.SetSizer(self.img_vert_sizer)
        self.Scrolling_to()


    def Re_Scale_Pal(self, i_min_max):
        print "rescale palette "
        self.i_min_max = i_min_max
        self.make_image(my_call_back = False)

    def onCheck_invert(self, event):
        ''' inverts color palette '''
        print "inverting color palette"
        evn_chek = event.Checked()
        print "event.Checked() =", event.Checked()
        print "type(event.Checked()) =", type(evn_chek)
        self.invert_img = evn_chek
        #self.intro_img_path()
        self.intro_img_path_2()
        wx.CallAfter(self.Scrolling_to)

    def onCheck_sqrt(self, event):
        ''' inverts square root '''
        print "inverting square root"
        evn_chek = event.Checked()
        print "event.Checked() =", event.Checked()
        print "type(event.Checked()) =", type(evn_chek)
        self.sqrt_scale_img = evn_chek

        #self.intro_img_path()
        self.intro_img_path_2()

        wx.CallAfter(self.Scrolling_to)

    def onLoadImgBtn(self, event):

        '''open dialog to open the experimental image'''

        lst_forms =  "*.img;*.sfrm;*.dm3;*.edf;*.xml;*.cbf;*.kccd;*.msk;"
        lst_forms += "*.spr;*.tif;*.h5;*.mccd;*.mar3450;*.mar2300;*.npy;"
        lst_forms += "*.tif;*.pnm;*.img;*.img;*.tif"

        dlg = wx.FileDialog(self, "Choose an image file", os.getcwd(),
                            "",lst_forms, wx.OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            tmp_file_path = dlg.GetPath()
            img2load = "anaelu_2_fabio_img.edf"

            convert_cmd  = "anaelu_fabio2edf"
            convert_cmd += " img_in=" + tmp_file_path
            convert_cmd += " img_out=" + img2load

            shell_func(convert_cmd, shell=True)
            self.img_path = img2load

            self.intro_img_path(path_to_img = self.img_path, use_fabio = True)
            self.intro_img_path_2()
            wx.CallAfter(self.Scrolling_to_2)

        else:
            print "No image Loaded"

        dlg.Destroy()

    def my_bindings(self):

        '''binding the mouse functions'''

        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def Scrolling_to(self):

        """setting the scrolling recognition to do the zoom-unzoom"""
        VwStart = self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y
        ClSize = self.GetClientSize()
        Vsize = self.GetVirtualSize()
        x_pos_start, y_pos_start = (VwStart[0] + ClSize[0], VwStart[1] + ClSize[1])

        if(self.Prnt_Wg.scroll_pos_x < 0):
            self.Prnt_Wg.scroll_pos_x = 0

        if(self.Prnt_Wg.scroll_pos_y < 0):
            self.Prnt_Wg.scroll_pos_y = 0

        if(x_pos_start > Vsize[0]):
            x_pos_start = Vsize[0]
            self.Prnt_Wg.scroll_pos_x = x_pos_start - ClSize[0]

        if(y_pos_start > Vsize[1]):
            y_pos_start = Vsize[1]
            self.Prnt_Wg.scroll_pos_y = y_pos_start - ClSize[1]

        self.Scrolling_to_2()

        # next function is added because sometimes it needs
        # to be called from outside alone

    def Scrolling_to_2(self):

        self.Scroll(int(self.Prnt_Wg.scroll_pos_x), int(self.Prnt_Wg.scroll_pos_y))
        self.Layout()
        self.Prnt_Wg.Layout()

    def OnLeftButDown(self, event):
        self.Bdwn = True
        #self.old_Mouse_Pos_x, self.old_Mouse_Pos_y = event.GetPosition()
        virt_x, virt_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        self.old_Mouse_Pos_x = virt_x - self.Prnt_Wg.scroll_pos_x
        self.old_Mouse_Pos_y = virt_y - self.Prnt_Wg.scroll_pos_y

    def OnLeftButUp(self, event):
        self.Bdwn = False

    def OnMouseMotion(self, event):
        virt_x, virt_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        self.Mouse_Pos_x = virt_x - self.Prnt_Wg.scroll_pos_x
        self.Mouse_Pos_y = virt_y - self.Prnt_Wg.scroll_pos_y

        '''
        print "Mouse_Pos_x, Mouse_Pos_y =", self.Mouse_Pos_x, self.Mouse_Pos_y
        print "virt_x, virt_y =", virt_x, virt_y
        '''

        #FIXME the magic 5.0 used in next two lines should be calculated properly
        x_pix = int((float(virt_x) / self.Prnt_Wg.img_scale) / 5.0)
        y_pix = int((float(virt_y) / self.Prnt_Wg.img_scale) / 5.0)

        try:
            label_str = "Pixel Intensity (" + str(x_pix)  + ", " \
                      + str(y_pix) +") = " + str(self.data2d[y_pix, x_pix])
            self.Prnt_Wg.label_data_info.SetLabelText(label_str)

        except IndexError:
            print"pointer moved from image"


    def OnMouseWheel(self, event):

        '''function to rescale the image from the wheel move'''

        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        v_size_x, v_size_y = self.GetVirtualSize()
        may_be_useful_in_the_future = '''
        #print "v_size_x, v_size_y =", v_size_x, v_size_y
        #print "self.GetRect            ", self.GetRect()
        #print "self.GetClientRect      ", self.GetClientRect()
        #print "self.GetClientSize      ", self.GetClientSize()

        #print "self.GetViewStart       ", self.GetViewStart()
        #print "self.GetBorder          ", self.GetBorder()
        #print "self.GetScrollThumb     ", self.GetScrollThumb()
        #print "self.GetScrollPos       ", self.GetScrollPos()
        #print "self.GetScrollPageSize  ", self.GetScrollPageSize()
        #print "self.GetScrollLines     ", self.GetScrollLines()
        #print "self.GetScrollRange     ", self.GetScrollRange()

        #print "GetViewStart()[0] + Mouse_Pos_x =", self.GetViewStart()[0] + self.Mouse_Pos_x
        '''

        corner_centered = '''
        self.x_scroll_uni = float( self.Prnt_Wg.scroll_pos_x) / float(v_size_x)
        self.y_scroll_uni = float( self.Prnt_Wg.scroll_pos_y) / float(v_size_y)
        '''

        # zooming centered in the center of visible area of the image
        client_siz_x, client_siz_y = self.GetClientSize()
        self.x_scroll_uni = float( self.Prnt_Wg.scroll_pos_x + client_siz_x / 2 ) / float(v_size_x)
        self.y_scroll_uni = float( self.Prnt_Wg.scroll_pos_y + client_siz_y / 2) / float(v_size_y)


        mouse_cetererd = '''
        self.x_scroll_uni = float( self.Prnt_Wg.scroll_pos_x + self.Mouse_Pos_x) / float(v_size_x)
        self.y_scroll_uni = float( self.Prnt_Wg.scroll_pos_y + self.Mouse_Pos_y) / float(v_size_y)
        #'''

    def OnIdle(self, event):

        '''math to do re rescaling of the image'''
        if( self.scroll_rot != 0 ):
            new_scale = self.Prnt_Wg.img_scale * ( 1.0 + self.scroll_rot * 0.01 )
            if( new_scale > 0.3 ):
                new_scale = 0.3

            elif( new_scale < 0.033333 ):
                new_scale = 0.033333

            if( new_scale != self.Prnt_Wg.img_scale ):
                self.Prnt_Wg.img_scale = new_scale
                self.Prnt_Wg.update_scale()

                v_size_x, v_size_y = self.GetVirtualSize()

                corner_centered = '''
                self.Prnt_Wg.scroll_pos_x = float(self.x_scroll_uni * v_size_x)
                self.Prnt_Wg.scroll_pos_y = float(self.y_scroll_uni * v_size_y)
                '''

                client_siz_x, client_siz_y = self.GetClientSize()
                self.Prnt_Wg.scroll_pos_x = float(self.x_scroll_uni * v_size_x) - client_siz_x / 2
                self.Prnt_Wg.scroll_pos_y = float(self.y_scroll_uni * v_size_y) - client_siz_y / 2

                mouse_cetererd = '''
                self.Prnt_Wg.scroll_pos_x = float(self.x_scroll_uni * v_size_x) - self.Mouse_Pos_x
                self.Prnt_Wg.scroll_pos_y = float(self.y_scroll_uni * v_size_y) - self.Mouse_Pos_y
                #'''

            self.scroll_rot = 0



        elif( (self.old_Mouse_Pos_x != -self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != -self.Mouse_Pos_y )and self.Bdwn == True):

            dx = self.Mouse_Pos_x - self.old_Mouse_Pos_x
            dy = self.Mouse_Pos_y - self.old_Mouse_Pos_y
            self.Prnt_Wg.scroll_pos_x = self.Prnt_Wg.scroll_pos_x - dx
            self.Prnt_Wg.scroll_pos_y = self.Prnt_Wg.scroll_pos_y - dy


        else:
            #TODO: Test from windows the behavior of
            #      the absent of next commented code
            tst_off = '''
            self.scroll_rot = 0
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y
            self.Prnt_Wg.update_scroll_pos()
            '''

        self.old_Mouse_Pos_x = self.Mouse_Pos_x
        self.old_Mouse_Pos_y = self.Mouse_Pos_y
        self.Prnt_Wg.update_scroll_pos()


class MultiImagePanel(wx.Panel):

    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(MultiImagePanel, self).__init__(parent)
        self.parent_widget = parent

        self.i_min_max = [None, None]

        main_panel_box = wx.BoxSizer(wx.VERTICAL)
        self.scrolled_panel_lst = []

        num_of_panels = parent.num_of_panels

        mid_panel_num = num_of_panels / 2
        for i in xrange(num_of_panels):
            self.scrolled_panel_lst.append(ScrolledImg(self))

        for nm, pnl in enumerate(self.scrolled_panel_lst):
            if(nm != 1):
                pnl.set_bmp_gen()

            else:
                pnl.set_bmp_gen("diff")

        for pnl in self.scrolled_panel_lst:
            pnl.intro_img_path(None)

        for pnl in self.scrolled_panel_lst:
            pnl.intro_img_path_3()

        img_right_panel_box = wx.BoxSizer(wx.HORIZONTAL)

        top_muliple_img_box = wx.BoxSizer(wx.VERTICAL)
        for singlepanel in  self.scrolled_panel_lst[0:mid_panel_num]:
            top_muliple_img_box.Add(singlepanel, 20, wx.EXPAND)

        bot_muliple_img_box = wx.BoxSizer(wx.VERTICAL)
        for singlepanel in  self.scrolled_panel_lst[mid_panel_num:num_of_panels]:
            bot_muliple_img_box.Add(singlepanel, 20, wx.EXPAND)

        img_right_panel_box.Add(top_muliple_img_box, 20, wx.EXPAND)
        img_right_panel_box.Add(bot_muliple_img_box, 20, wx.EXPAND)

        self.label_data_info = wx.StaticText(self, wx.ID_ANY, ' No Data to display yet  ')
        main_panel_box.Add(img_right_panel_box, 20, wx.EXPAND)
        main_panel_box.Add(self.label_data_info)

        self.SetSizer(main_panel_box)

    def Refr_Imgs(self):
        i_min = 0
        i_max = 1
        i_min_max = [i_min, i_max]
        for nm, pnl in enumerate(self.scrolled_panel_lst):
            if(pnl.img is not None and nm != 1):
                if(pnl.local_min_max[0] < i_min_max[0]):
                    i_min_max[0] = pnl.local_min_max[0]

                if(pnl.local_min_max[1] > i_min_max[1]):
                    i_min_max[1] = pnl.local_min_max[1]

        for nm, pnl in enumerate(self.scrolled_panel_lst):
            if(pnl.img is not None and nm != 1):
                pnl.Re_Scale_Pal(i_min_max)

        self.update_scale()

    def update_scroll_pos(self):
        for pnl in self.scrolled_panel_lst:
            if(pnl.img is not None):
                pnl.Scrolling_to()

    def update_scale(self):
        for pnl in self.scrolled_panel_lst:
            if(pnl.img is not None):
                pnl.set_img_n_bmp()

class dummy_obj(wx.Frame):
    def __init__(self):
        super(dummy_obj, self).__init__( None, -1, "ANAELU 2.0", size = (1200,600))
        self.num_of_panels = 4
        self.imgs_panel = MultiImagePanel(self)
        main_panel_box = wx.BoxSizer(wx.VERTICAL)
        main_panel_box.Add(self.imgs_panel, 20, wx.EXPAND)

        self.button =wx.Button(self, label="Go, do it", pos=(200, 125))
        self.button.Bind(wx.EVT_BUTTON, self.OnButton)
        main_panel_box.Add(self.button, 2)

        self.SetSizer(main_panel_box)
        self.Show()

    def OnButton(self, event):
        print "clicked"


if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    frame = dummy_obj()
    frame.Show()
    wxapp.MainLoop()
