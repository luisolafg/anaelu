#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import wx.lib.scrolledpanel as scroll_pan
from subprocess import call as shell_func

from readcfl import *
from sample_n_cfl_panels import *
from instrument_param_panel import *
from scrollable_multipanel import *

from grid_atoms_xyz import AtomsPanel

from np_nditer_RGB.bitmap_from_numpy_hot_colour_palete import build_np_img
from pop_maks_tool import MyFrame

class DataIntroPanel(scroll_pan.ScrolledPanel):

    '''gathering all single panels in one'''
    def __init__(self, parent):
        super(DataIntroPanel, self).__init__(parent)

        self.parent_wdgt = parent
        self.crystal_panel = CrystalPanel(self)
        self.policrystal_panel = PoliCrystalPanel(self)
        self.param_panel = ParamPanel(self)
        self.atom_panel = AtomsPanel(self)

        input_scale_factor_sizer = wx.BoxSizer(wx.HORIZONTAL)
        label_scale_factor = wx.StaticText(self, wx.ID_ANY, '     Scale Factor')
        self.input_scale_data = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        self.input_scale_data.SetValue('1.0')
        input_scale_factor_sizer.Add(label_scale_factor)
        input_scale_factor_sizer.Add(self.input_scale_data)

        input_smooth_times_sizer = wx.BoxSizer(wx.HORIZONTAL)
        label_times_factor = wx.StaticText(self, wx.ID_ANY, '     Times to Smooth')
        self.input_times_data = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        self.input_times_data.SetValue('15')
        input_smooth_times_sizer.Add(label_times_factor)
        input_smooth_times_sizer.Add(self.input_times_data)

        RunBtn  = wx.Button(self, wx.ID_ANY, 'Run Model\n')
        self.Bind(wx.EVT_BUTTON, self.Run, RunBtn)
        MaskBtn  = wx.Button(self, wx.ID_ANY, "Mask's Model\n")
        self.Bind(wx.EVT_BUTTON, self.Mask, MaskBtn)
        CutpeaksBtn  = wx.Button(self, wx.ID_ANY, "Cut Mask's Peaks\n")
        self.Bind(wx.EVT_BUTTON, self.Cutpeaks, CutpeaksBtn)
        SmoothbckgrBtn  = wx.Button(self, wx.ID_ANY, ' Background \n Smoothing')
        self.Bind(wx.EVT_BUTTON, self.Smoothbckgr, SmoothbckgrBtn)
        ScaleBtn  = wx.Button(self, wx.ID_ANY, 'Scale, Sum Background\n         and Difference')
        self.Bind(wx.EVT_BUTTON, self.Scale, ScaleBtn)
        RBtn  = wx.Button(self, wx.ID_ANY, 'Re-model and Comparison\n')
        self.Bind(wx.EVT_BUTTON, self.R, RBtn)
        MarkBtn = wx.Button(self, wx.ID_ANY, 'Pop Mark Tool\n')
        self.Bind(wx.EVT_BUTTON, self.mark_pop, MarkBtn)

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(self.crystal_panel)
        main_sizer.Add(self.atom_panel, 1, wx.EXPAND ) # atoms grid
        main_sizer.Add(self.policrystal_panel)
        main_sizer.Add(self.param_panel)

        maskminSizer   = wx.BoxSizer(wx.HORIZONTAL)
        self.label_mask_min = wx.StaticText(self, wx.ID_ANY, '     Mask Threshold')
        self.input_mask_min = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        self.input_mask_min.SetValue("0.5")
        maskminSizer.Add(self.label_mask_min, 0, wx.EXPAND)
        maskminSizer.Add(self.input_mask_min, 0, wx.EXPAND)

        btns_sizer = wx.BoxSizer(wx.VERTICAL)

        #######################################################################################

        output_r_sizer = wx.BoxSizer(wx.HORIZONTAL)

        label_r_out = wx.StaticText(self, wx.ID_ANY, 'R = ')
        self.output_r = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        self.output_r.SetValue('None')

        output_r_sizer.Add(label_r_out)
        output_r_sizer.Add(self.output_r)

        #######################################################################################
        tmp_off = '''
        output_r_2_sizer = wx.BoxSizer(wx.HORIZONTAL)

        label_r_2_out = wx.StaticText(self, wx.ID_ANY, 'R ** 2 = ')
        self.output_r_2 = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        self.output_r_2.SetValue('None')

        output_r_2_sizer.Add(label_r_2_out)
        output_r_2_sizer.Add(self.output_r_2)
        '''
        #######################################################################################

        btns_sizer.Add(RunBtn, 0, wx.EXPAND)
        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)

        mask_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        mask_h_sizer.Add(maskminSizer, 0, wx.ALL| wx.CENTER, 2)
        mask_h_sizer.Add(MaskBtn, 1, wx.EXPAND)

        btns_sizer.Add(mask_h_sizer, 0, wx.EXPAND)

        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)

        cut_h_box = wx.BoxSizer(wx.HORIZONTAL)
        cut_h_box.Add(wx.StaticText(self, wx.ID_ANY, '  '), 1, wx.EXPAND)
        cut_h_box.Add(CutpeaksBtn, 1, wx.EXPAND)
        btns_sizer.Add(cut_h_box, 0, wx.EXPAND)

        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        smoth_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        smoth_h_sizer.Add(input_smooth_times_sizer, 0, wx.ALL| wx.CENTER, 2)
        smoth_h_sizer.Add(SmoothbckgrBtn, 1, wx.EXPAND)
        btns_sizer.Add(smoth_h_sizer, 0, wx.EXPAND)

        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)

        scale_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        scale_h_sizer.Add(input_scale_factor_sizer, 0, wx.ALL| wx.CENTER, 2)
        scale_h_sizer.Add(ScaleBtn, 1, wx.EXPAND)
        btns_sizer.Add(scale_h_sizer, 0, wx.EXPAND)

        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        smoth_h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btns_sizer.Add(RBtn, 0, wx.EXPAND)
        btns_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        btns_sizer.Add(output_r_sizer, 0, wx.ALL| wx.CENTER, 2)

        num_of_panels = parent.num_of_panels
        self.panel_btn_lst = []
        self.chk_invert_lst = []
        self.chk_sqrt_lst = []
        mid_panel_num = num_of_panels / 2
        img_control_box = wx.BoxSizer(wx.HORIZONTAL)
        top_muliple_img_box = wx.BoxSizer(wx.VERTICAL)
        bot_muliple_img_box = wx.BoxSizer(wx.VERTICAL)

        for p_label  in [("Inverted Colours    ", "sqrt(i)"),
                         ("(Green / Blue) Palette", "(Dark / Bright) Palette"),
                         ("Inverted Colours    ", "sqrt(i)"),
                         ("Inverted Colours    ", "sqrt(i)"),]:

            tmp_lab = "  Load XRD image"
            tmp_btn = wx.Button(self, wx.ID_ANY, tmp_lab)
            self.panel_btn_lst.append(tmp_btn)
            tmp_chk_1 = wx.CheckBox(self, label=p_label[0])
            tmp_chk_2 = wx.CheckBox(self, label=p_label[1])
            self.chk_invert_lst.append(tmp_chk_1)
            self.chk_sqrt_lst.append(tmp_chk_2)

        for widget_num in xrange(2):
            top_muliple_img_box.Add(wx.StaticText(self, wx.ID_ANY, '  -  '),
                                    0, wx.EXPAND)
            top_muliple_img_box.Add(self.panel_btn_lst[widget_num], 0, wx.EXPAND)
            top_muliple_img_box.Add(self.chk_invert_lst[widget_num], 0, wx.EXPAND)
            top_muliple_img_box.Add(self.chk_sqrt_lst[widget_num], 0, wx.EXPAND)

        for widget_num in xrange(2, 4):
            bot_muliple_img_box.Add(wx.StaticText(self, wx.ID_ANY, '  -  '),
                                    0, wx.EXPAND)
            bot_muliple_img_box.Add(self.panel_btn_lst[widget_num], 0, wx.EXPAND)
            bot_muliple_img_box.Add(self.chk_invert_lst[widget_num], 0, wx.EXPAND)
            bot_muliple_img_box.Add(self.chk_sqrt_lst[widget_num], 0, wx.EXPAND)

        img_control_box.Add(top_muliple_img_box, 0, wx.EXPAND)
        img_control_box.Add(bot_muliple_img_box, 0, wx.EXPAND)

        main_sizer.Add(btns_sizer, 0, wx.EXPAND)
        main_sizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        main_sizer.Add(MarkBtn, 0, wx.EXPAND)
        main_sizer.Add(img_control_box, 0, wx.CENTER)

        main_sizer.Fit(self)
        self.SetSizer(main_sizer)

        self.SetupScrolling()
        self.Layout()

    def onRunCfl(self, event):

        '''getting all the .cfl values (crystal and polycristal) from the gui and inserting them in new variables'''
        new_input_lg_size  = self.policrystal_panel.input_lg_size.GetValue()
        new_avg_strain     = self.policrystal_panel.avg_strain.GetValue()
        new_input_h        = self.policrystal_panel.input_h.GetValue()
        new_input_k        = self.policrystal_panel.input_k.GetValue()
        new_input_l        = self.policrystal_panel.input_l.GetValue()
        new_input_hkl_wh   = self.policrystal_panel.input_hkl_wh.GetValue()
        new_input_azm_ipf  = self.policrystal_panel.input_azm_ipf.GetValue()
        new_input_ipf_res  = self.policrystal_panel.input_ipf_res.GetValue()
        #new_input_mask_min = self.policrystal_panel.input_mask_min.GetValue()
        new_input_mask_min = self.input_mask_min.GetValue()

        dto = CflData() # creating a new variable to insert all the .cfl values

        dto.lg_size         =  new_input_lg_size
        dto.new_avg_strain  =  new_avg_strain
        dto.h               =  new_input_h
        dto.k               =  new_input_k
        dto.l               =  new_input_l
        dto.hkl_wh          =  new_input_hkl_wh
        dto.azm_ipf         =  new_input_azm_ipf
        dto.ipf_res         =  new_input_ipf_res
        dto.mask_min        =  new_input_mask_min

        new_inputtitle  =   self.crystal_panel.inputtitle.GetValue()
        new_input_spgr  =   self.crystal_panel.input_spgr.GetValue()
        new_input_a     =   self.crystal_panel.input_a.GetValue()
        new_input_b     =   self.crystal_panel.input_b.GetValue()
        new_input_c     =   self.crystal_panel.input_c.GetValue()
        new_input_alpha =   self.crystal_panel.input_alpha.GetValue()
        new_input_beta  =   self.crystal_panel.input_beta.GetValue()
        new_input_gamma =   self.crystal_panel.input_gamma.GetValue()

        dto.new_inputtitle =  new_inputtitle
        dto.spgr           =  new_input_spgr
        dto.a              =  new_input_a
        dto.b              =  new_input_b
        dto.c              =  new_input_c
        dto.alpha          =  new_input_alpha
        dto.beta           =  new_input_beta
        dto.gamma          =  new_input_gamma

        atm_lst = self.atom_panel.myGrid.get_my_values()

        my_file = write_cfl(dto, do_close = False)
        self.atom_panel.write_cfl_athom( dto_lts = atm_lst, myfile = my_file)

    def onOpenCfl(self, event):

        '''opening the .cfl file'''
        dlg = wx.FileDialog(self, "Choose a \'.cfl\' file", os.getcwd(),
                            "", "*.cfl;*.cif", wx.OPEN) # filtering cfl and cif

        if dlg.ShowModal() == wx.ID_OK:
            mypath = str(dlg.GetPath())

            if( mypath[-3:] == "cif" ):
                print "running cif2cfl convert"
                to_run = "anaelu_cif2cfl " + mypath
                shell_func(to_run, shell=True)
                my_cfl_path = "my_CFL_file.cfl"

            else:
                my_cfl_path = mypath

            my_cfl_content = read_cfl_file(my_file_path = my_cfl_path)
            self.crystal_panel.set_values(my_cfl_content)
            self.policrystal_panel.set_values(my_cfl_content)

            if(my_cfl_content.mask_min != None ):
                self.input_mask_min.SetValue(my_cfl_content.mask_min)

            self.atom_panel.OnLoadButt(event, my_cfl_path)

        else:
            print "No cfl file to open"

        dlg.Destroy()

    def onRunDat(self, event):

        '''getting all the param.dat values from the gui and inserting them in new variables'''
        new_input_lambd    = self.param_panel.input_lambd.GetValue()
        new_input_dst_det  = self.param_panel.input_dst_det.GetValue()
        new_input_diam_det = self.param_panel.input_diam_det.GetValue()
        new_input_x_beam   = self.param_panel.input_x_beam.GetValue()
        new_input_y_beam   = self.param_panel.input_y_beam.GetValue()
        new_input_x_res    = self.param_panel.input_x_res.GetValue()
        new_input_y_res    = self.param_panel.input_y_res.GetValue()
        new_input_up_hfl   =  self.param_panel.chk_up_hfl.GetValue(), "\n\n"

        dto = InstrumentData() # creating a new variable to insert all the .dat values
        dto.lambd    =  new_input_lambd
        dto.dst_det  =  new_input_dst_det
        dto.diam_det =  new_input_diam_det
        dto.x_beam   =  new_input_x_beam
        dto.y_beam   =  new_input_y_beam
        dto.x_res    =  new_input_x_res
        dto.y_res    =  new_input_y_res

        tmp_T_o_F = str(new_input_up_hfl)[1]

        if(tmp_T_o_F == "T"):
            dto.up_hfl   =  True

        else:
            dto.up_hfl   =  False

        write_dat(dto)

    def onOpenDat(self, event):

        '''opening the param.dat file'''
        dlg = wx.FileDialog(self, "Choose a PARAMETER file", os.getcwd(), "", "*.dat*", wx.OPEN) #filtering only .dat files

        if dlg.ShowModal() == wx.ID_OK:
            mypath = str(dlg.GetPath())
            my_dat_content = read_dat_file(my_file_path = mypath)
            '''setting the experiment params values on the gui'''
            self.param_panel.input_lambd.SetValue(my_dat_content.lambd)
            self.param_panel.input_dst_det.SetValue(my_dat_content.dst_det)
            self.param_panel.input_diam_det.SetValue(my_dat_content.diam_det)
            self.param_panel.input_x_beam.SetValue(my_dat_content.x_beam)
            self.param_panel.input_y_beam.SetValue(my_dat_content.y_beam)
            self.param_panel.input_x_res.SetValue(my_dat_content.x_res)
            self.param_panel.input_y_res.SetValue(my_dat_content.y_res)
            self.param_panel.chk_up_hfl.SetValue(my_dat_content.up_hfl)

        else:
            print "No dat file to open"

        dlg.Destroy()


    def mark_pop(self, event):
        scr_panel0 = self.parent_wdgt.imgs_panel.scrolled_panel_lst[0]
        #physical size of view area
        ClSizeX = scr_panel0.GetClientSize()[0]
        ClSizeY = scr_panel0.GetClientSize()[1]

        #physical size of all image area
        VsizeX = scr_panel0.GetVirtualSize()[0]
        VsizeY = scr_panel0.GetVirtualSize()[1]
        VwStartX = self.parent_wdgt.imgs_panel.scroll_pos_x
        VwStartY = self.parent_wdgt.imgs_panel.scroll_pos_y

        n_y_arr = np.size( scr_panel0.data2d[0:1, :] )
        n_x_arr = np.size( scr_panel0.data2d[:, 0:1] )

        #TODO make sure you didn't know scale_x and scale_y already
        scale_x = float(n_x_arr) / float(VsizeX)
        scale_y = float(n_y_arr) / float(VsizeY)

        scale_avg = ( scale_x + scale_y ) / 2.0

        self.x_view_from = int(float(VwStartX) * scale_avg)
        if(self.x_view_from < 0):
            self.x_view_from = 0

        self.x_view_to = int(float(VwStartX + ClSizeX) * scale_avg)
        if(self.x_view_to > n_x_arr):
            self.x_view_to = n_x_arr

        self.y_view_from = int(float(VwStartY) * scale_avg)
        if(self.y_view_from < 0):
            self.y_view_from = 0

        self.y_view_to = int(float(VwStartY + ClSizeY) * scale_avg)
        if(self.y_view_to > n_y_arr):
            self.y_view_to = n_y_arr

        data2d = scr_panel0.data2d[self.y_view_from:self.y_view_to, self.x_view_from:self.x_view_to]
        #TODO maybe is better to calculate this
        n_y = np.size( data2d[0:1, :] )
        n_x = np.size( data2d[:, 0:1] )

        arr_avgr = float(n_y + n_x)
        my_avgr = 850.0

        fr_scale = my_avgr / arr_avgr
        fr_dim = (int(n_y * fr_scale) + 200, int(n_x * fr_scale) + 50)

        self.frame = MyFrame(fr_dim)
        self.frame.build_in(data2d, fr_scale, self)
        self.frame.Show()

    def app_mark_tool(self):

        cmd_lst = []
        for rect_uni in self.frame.lst_out:
            print "rect_uni =", rect_uni
            if(rect_uni[0] > rect_uni[2]):
                x1 = rect_uni[2]
                x2 = rect_uni[0]

            else:
                x1 = rect_uni[0]
                x2 = rect_uni[2]

            if(rect_uni[1] > rect_uni[3]):
                y1 = rect_uni[3]
                y2 = rect_uni[1]

            else:
                y1 = rect_uni[1]
                y2 = rect_uni[3]

            x1 = self.x_view_from + x1
            y1 = self.y_view_from + y1
            x2 = self.x_view_from + x2
            y2 = self.y_view_from + y2
            cmd_lst.append((x1,y1,x2,y2))

        self.parent_wdgt.OnMark_ex(cmd_lst)

    def Run(self, event, repaint = True):

        '''saving all the data from the gui  in their respective files'''
        self.onRunCfl(event)
        self.onRunDat(event)
        self.parent_wdgt.OnRun(event, repaint)

    def Mask(self, event, repaint = True):

        '''saving all the data from the gui  in their respective files'''
        self.onRunCfl(event)
        self.onRunDat(event)
        self.parent_wdgt.OnMask(event, repaint)

    def Cutpeaks(self, event, repaint = True):
        self.parent_wdgt.OnCutpeaks(event, repaint)

    def Smoothbckgr(self, event, repaint = True):
        self.parent_wdgt.OnSmoothbckgr(event, repaint)

    def Scale(self, event, repaint = True):
        self.parent_wdgt.OnScale(event, False)
        self.parent_wdgt.OnSumbckgr(event, False)
        self.parent_wdgt.OnR(event)

    def R(self, event):
        self.onRunCfl(event)
        self.onRunDat(event)
        self.parent_wdgt.OnRun(event, False)
        self.Scale(event, True)

class MainDataPanel(wx.Frame):

    '''the main panel for the input data'''
    def __init__(self):
        super(MainDataPanel, self).__init__( None, -1, 'Input Data')

        self.main_panel = DataIntroPanel(self)
        MainSizer = wx.BoxSizer(wx.VERTICAL)
        #MainSizer.Add(self.main_panel, proportion = 1, flag = wx.ALL|wx.EXPAND,border = 5)
        MainSizer.Add(self.main_panel, proportion = 1, flag = wx.EXPAND)
        self.SetSizer(MainSizer)
        self.Centre()

if __name__ == '__main__':
    app = wx.App()
    frame = MainDataPanel().Show()
    app.MainLoop()
