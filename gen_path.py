import sys
from distutils import sysconfig

if( __name__ == "__main__" ):
    all_args = sys.argv
    print "arguments passed to the setpath generator:", all_args
    locl_path = all_args[1]

    if(len(all_args) > 2):
        inc_path = sysconfig.get_python_inc()
        print "\n\n sysconfig.get_python_inc() =", inc_path
        lst_inc_path = inc_path.split("/")
        miniconda_path = "/".join(lst_inc_path[0:-2])
        print "path for miniconda installation:", miniconda_path
        py2_bin = miniconda_path + "/bin:"

    else:
        py2_bin = ""

    print "local path =", locl_path
    anaelu_app_path = locl_path + "/build_apps"

    full_path_2add = py2_bin + anaelu_app_path

    print "anaelu app path =", anaelu_app_path

    myfile = open("setpath.sh", "w")

    wrstring = "export PATH=" + full_path_2add + ":$PATH"  + "\n"
    myfile.write(wrstring);

    wrstring = "export ANAELU_PATH=" + anaelu_app_path + "\n"
    myfile.write(wrstring);

    myfile.write("\n")

    myfile.close()

