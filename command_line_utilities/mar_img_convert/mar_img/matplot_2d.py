import numpy
import sys
from matplotlib import pyplot as plt

if len(sys.argv)-1 == 0:
    print("Error. No file to open")
    exit()
else:
    opn_fil_nm = sys.argv[1]

myfile = open(opn_fil_nm, "r")

all_lines = myfile.readlines()
myfile.close()
n=0

xres=2300
yres=2300
a=numpy.zeros((xres,yres))

x=0
y=0
for line in all_lines:
  n+=1
  if n>6:
    #print x,y
    a[x,y]=float(line)
    if x>=xres-1:
      x=-1
      y+=1
    x+=1
  else:
    print n, line

print "Plotting a"
a[0:100,0:500] = 5500.0
#plt.imshow(  numpy.transpose(a) , interpolation = "nearest" )
plt.imshow(  a , interpolation = "nearest" )
plt.show()
