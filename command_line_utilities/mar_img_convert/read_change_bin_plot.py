import numpy
from matplotlib import pyplot as plt
#from scipy.io.numpyio import fread

def read_file(path_to_img = "Test.bin"):

    print "reading ", path_to_img, " file"

    xres = 2300
    yres = 2300

    #data_in = numpy.fromfile(path_to_img, dtype=numpy.uint16)
    #data_in = numpy.fromfile(path_to_img, dtype=numpy.float64)
    #read_data = read_data.reshape((n, m), order="FORTRAN")


    #data_in = numpy.fromfile(path_to_img, dtype=numpy.float32)
    #data_in = numpy.fromfile("/home/lui/f90lafg_test/2d_pat_1.raw", dtype=numpy.float64)
    data_in = numpy.fromfile(path_to_img, dtype=numpy.float64)


    print len(data_in)
    #read_data = data_in.reshape((xres, yres), order="FORTRAN").T
    read_data = data_in.reshape((xres, yres))
    return read_data

def plott_img(arr):
    print "Plotting arr"
    plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    #plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    img_arr = read_file("./mar_img/APT73_d122_from_m02to02__01_41.mar2300.raw")
    #img_arr = read_file("../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.bin")
    plott_img(img_arr)
