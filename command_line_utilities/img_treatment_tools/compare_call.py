import tools
#from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys
from arg_interface import get_par

if( __name__ == "__main__" ):
    par_def =(("img_calc_in", "my_calc_in.edf"),
              ("img_exp_in", "my_exp_in.edf"),
              ("dif_img_out", "diff_img.edf"),
              ("res_data_out", "img_diff_info.dat"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])
    #print "in_par =", in_par

    path_to_img_1 = in_par[0][1]
    path_to_img_2 = in_par[1][1]
    img_file_out =  in_par[2][1]
    res_file_out =  in_par[3][1]

    #TODO rename variables to "experimental" and "calculated"
    #     since the order of the images is relevant to calculate
    #     the R

    #another TODO
    #verify that the correct order is used for calculating the
    #value of R

    img_1_in = fabio.open(path_to_img_1).data.astype(np.float64)
    img_2_in = fabio.open(path_to_img_2).data.astype(np.float64)
    xres, yres = np.shape(img_1_in)
    x_tst, y_tst = np.shape(img_2_in)
    if(xres == x_tst and yres == y_tst):

        tools.inner_tools.resid(img_1_in, img_2_in)
        arr_out = tools.inner_tools.img_out
        r_out = tools.inner_tools.r
        r_2_out = tools.inner_tools.r_2

        res_img = fabio.edfimage.edfimage()
        res_img.data = arr_out

        print "img_file_out =", img_file_out
        res_img.write(str(img_file_out))

        r_str = "r = " + str(r_out)
        r_2_str = "r_2 = " + str(r_2_out)
        myfile = open(str(res_file_out), "w")
        myfile.write(r_str + "\n" + r_2_str + "\n")
        myfile.close()

    else:
        print "images with wrong sizes"






