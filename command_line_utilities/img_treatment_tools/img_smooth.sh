#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo "entered $1 as file input"
echo "entered $2 as output"
echo "entered $3 as number of iterations"
python $ANAELU_PATH/../command_line_utilities/img_treatment_tools/smooth_call.py $1 $2 $3
