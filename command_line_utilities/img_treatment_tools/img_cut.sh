# calling python script that cuts peaks from images
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo "entered $1 as file input 1"
echo "entered $2 as file input 2"
echo "entered $3 as file input 3"
python $ANAELU_PATH/../command_line_utilities/img_treatment_tools/cut_call.py $1 $2 $3
echo "Done"
