#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#echo " "
#echo "removing previous builds"
#echo " "
#rm *.so *.pyc
echo "building with f2py"
f2py -c -m cut  cut.f90
echo " "


