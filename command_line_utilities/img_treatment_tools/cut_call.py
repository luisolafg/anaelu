#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import cut
import numpy as np
import fabio
import sys

from arg_interface import get_par

if( __name__ == "__main__" ):

    par_def =(("img_in", "my_img_in.edf"),
              ("mask_in", "my_mask_in.edf"),
              ("img_out", "cut_img.edf"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])

    path_to_img = in_par[0][1]
    path_to_mask = in_par[1][1]
    path_to_img_out =  in_par[2][1]

    try:

        ini_img = fabio.open(path_to_mask).data.astype(np.float64)

        ini_img2 = fabio.open(path_to_img).data.astype(np.float64)
        xres, yres = np.shape(ini_img2)

        cut.cutting.cut_img(ini_img, ini_img2, xres / 2.0, yres / 2.0)
        arr_out = cut.cutting.img_out
        new_img = fabio.edfimage.edfimage()

        new_img.data = arr_out
        path_to_img_out = path_to_img_out
        new_img.write(path_to_img_out)

    except:

        print "error ...\n\n"
        print "entered", sys.argv[1:]

        print "par_def =", par_def
