import tools
#from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys
from arg_interface import get_par

if( __name__ == "__main__" ):

    par_def =(("img_in_1", "my_img_in_1.edf"),
              ("img_in_2", "my_img_in_2.edf"),
              ("img_out", "img_sum.edf"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])
    #print "in_par =", in_par

    path_to_img_1 = in_par[0][1]
    path_to_img_2 = in_par[1][1]
    img_file_out =  in_par[2][1]

    img_1_in = fabio.open(path_to_img_1).data.astype(np.float64)
    img_2_in = fabio.open(path_to_img_2).data.astype(np.float64)
    xres, yres = np.shape(img_1_in)
    x_tst, y_tst = np.shape(img_2_in)
    if(xres == x_tst and yres == y_tst):

        tools.inner_tools.suma_img(img_1_in, img_2_in)
        arr_out = tools.inner_tools.img_out

        res_img = fabio.edfimage.edfimage()
        res_img.data = arr_out

        print "img_file_out =", img_file_out
        res_img.write(str(img_file_out))

    else:
        print "images with wrong sizes"






