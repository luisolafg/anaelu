import numpy as np
from matplotlib import pyplot as plt

def read_np_arr(f_name = None):
    lst_x_1 = []
    lst_y_1 = []
    lst_x_2 = []
    lst_y_2 = []
    lst_x_3 = []
    lst_y_3 = []

    with open(f_name) as fl:
        for ln_num, line in enumerate(fl):
            if(ln_num > 1 and ln_num < 721 ):
                lst_x_1.append(float(line[2:18]))
                lst_y_1.append(float(line[24:]))

            elif( ln_num > 725 and ln_num <  1444 ):
                lst_x_2.append(float(line[2:18]))
                lst_y_2.append(float(line[24:]))

            elif( ln_num >  1448  and ln_num < 2167 ):
                lst_x_3.append(float(line[2:18]))
                lst_y_3.append(float(line[24:]))


    np_x_1 = np.asarray(lst_x_1)
    np_y_1 = np.asarray(lst_y_1)

    np_x_2 = np.asarray(lst_x_2)
    np_y_2 = np.asarray(lst_y_2)

    np_x_3 = np.asarray(lst_x_3)
    np_y_3 = np.asarray(lst_y_3)

    plt.plot(np_x_1, np_y_1)
    plt.plot(np_x_2, np_y_2)
    plt.plot(np_x_3, np_y_3)
    plt.show()



if( __name__ == "__main__"):
    print "\n in __main__"
    read_np_arr("Debye_profile.dat")

