#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
python $ANAELU_BUILD/../anaelu_gui/img_scrollable.py
# uncomment next line and comment previous line if you wanna use dials python
#dials.python $ANAELU_BUILD/../anaelu_gui/img_scrollable.py
