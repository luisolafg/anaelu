#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import numpy
from matplotlib import pyplot as plt

def read_file(path_to_img = "2d_pat_1.asc"):

    print "readin ", path_to_img, " file"

    myfile = open(path_to_img, "r")

    all_lines = myfile.readlines()
    myfile.close()
    n=0

    #copied_from_eduardos_code = '''
    xres=2300
    yres=2300

    n_line = 0

    for line in all_lines:
        n_line += 1
        if( n_line < 10 ):
            print "____________________________________________________"

            print line
            print "comparing:"
            print line[:32].lower()
            print "with:"
            print " number of pixels in x direction"
        if( line[:32].lower() == " number of pixels in x direction" ):
            print "Found the X resolution"
            print "asc to convert =", line[36:]
            xres = int(line[36:])
            print " X =", xres

        elif( line[:32].lower() == " number of pixels in y direction" ):
            print "Found the Y resolution"
            print "asc to convert =", line[36:]
            yres = int(line[36:])
            print " Y =", yres


    #'''


    a=numpy.zeros((xres,yres))

    x=0
    y=yres - 1
    for line in all_lines:
        n+=1
        if n>6:
            a[x,y]=float(line)
            if x>=xres-1:
                x = -1
                y -= 1
            x += 1
        else:
            print n, line
    return a


def plott_img(arr):
    print "Plotting arr"
    plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    #plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    img_arr = read_file("2d_pat_1.asc")
    plott_img(img_arr)
