#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo " "
echo "Building CIF to CFL converter"
echo " "
cd crysfml_snapshot_n02_nov_2017/
export CRYSFML=$(pwd)

cd ../format_tools
./make_cif2cfl.sh
echo " "
echo "done building CIF to CFL converter "
